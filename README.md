# GitBook

This gitbook will be used to provide information and tutorial materials to ECMC schools so that they know what data they will need to provide for the ECMC data warehouse.

## Running the GitBook

First clone the repository.

```
git clone git@gitlab.com:damour/gitbook.git
```

Make sure that you have the gitbook node package installed.

```
npm install gitbook-cli -g
```

After you have a local repository for the book you can serve it locally at `http://localhost:4000`.

```
gitbook serve
```

## Modifying the GitBook Pages

Simply find and modify the `.md` file within the `docs` folder that corresponds to the page you would like to make changes to.

If any book variables need to be changed or added, those are defined in the `book.json` file.
